
var webpackConfig = require('../webpack.config')

module.exports = function (config) {
  config.set({
    browsers: ['PhantomJS'],
    frameworks: ['mocha','sinon-chai'],
    reporters: ['mocha'],
    files: ['./index.js'],
    preprocessors: {
      './index.js': ['webpack', 'sourcemap']
    },
    // logLevel: config.LOG_DEBUG, // for karma debug
    webpack: webpackConfig,
    webpackMiddleware: {
      noInfo: true
    }    
  })
}