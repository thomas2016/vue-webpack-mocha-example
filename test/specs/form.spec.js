import Vue from "vue";
import ElementUI from 'element-ui';
import Form from "../../src/components/form.vue";

describe("Form.view testing" , () => {
    before(() =>{
        Vue.use(ElementUI)
    });
    
    it("Compoment Render" , () => {                
        const Constructor = Vue.extend(Form);
        const vm = new Constructor().$mount();     
        expect(vm.user.name).to.equal('Thomas');
        expect(vm.$refs.username.value).to.equal('Thomas');
    });
    
    it("Compoment two-way binding" , () => {                
        const Constructor = Vue.extend(Form);
        const vm = new Constructor().$mount();
        vm.$refs.username.$emit('input', "Thomas Lin");
        expect(vm.user.name).to.equal('Thomas Lin');
    });
    
    // BDD trigger me
    it("Component methed invoke" , () => {
        const Constructor = Vue.extend(Form);
        const instance = new Constructor();
        let spy = sinon.spy(instance , "onSubmit");
        
        // // for debug to validate spy === object.method
        // console.debug(instance.onSubmit === spy)
        
        const vm = instance.$mount();
        vm.$refs.submit_button.$emit('click');        
        expect(spy).to.have.been.called;
    });

});